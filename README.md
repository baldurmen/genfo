genfo is a command-line tool templating engine that uses FLAC metadata. You
can use it to build album descriptions automatically.

# Installation

## With setup.py

You can install `genfo` like any other python program with the help of
`setuptools`:

    $ python3 setup.py install

## Manually

Since `genfo` is a single python file, you can call you can it directly:

    $ python3 /path/to/genfo.py

If like me you want to use this program on a daily basis, it's a good idea
to modify your `.bashrc` (normally in your home folder) to include this:

> \# Alias for genfo
>
> alias genfo="/path/to/genfo.py"

You then have to run `chmod +x /path/to/genfo.py` and
`source /path/to/.bashrc` to be able to call the program by simply typing
`genfo`.

# Dependencies

You will need these to run genfo correctly:

* python3
* docopt
* metaflac

If you are using FLAC files, metaflac should already be installed by default.
On Debian distros just type this to install the dependencies:

`sudo aptitude install python3 python3-docopt flac`

# How to use genfo

I wrote genfo because I wanted to create NFO file for whole
FLAC albums. To use genfo, simply add the right tags in a template file and
run it.

For ease of use, I included a `template.nfo` file. If you don't like it
or want something more fancy (ASCII ART!!), simply include the tags in
your new file and they will be replaced by the correct values.

example: `genfo "%n - %t" "/my/new/creative-common/album" "template.nfo" "I Rock.nfo"`

## CLI options

<pre>
Usage:
    genfo.py &lt;scheme&gt; &lt;directory&gt; &lt;template&gt; &lt;output&gt;
    genfo.py (-h | --help)
    genfo.py --version

Arguments:
    &lt;directory&gt;     The directory containing the album
    &lt;scheme&gt;        The tracklist scheme to use
    &lt;template&gt;      The blank NFO file to be filled
    &lt;output&gt;        The NFO file generated

Options:
    -h  --help       Shows the help screen
    --version        Outputs version information

    These are the tags you can use in the template file:

      {artist}       Name of the artist
      {album}        Name of the album
      {date}         Current date
      {genre}        Genre of the album
      {tracklist}    The tracklisting (including tracknumber,
                     playing time, title and size)
      {total_size}   Total size in MB of the album
      {total_time}   Total playing time
      {version}      Version of flac your are using

    These are the options you can use to define the tracklist scheme:
      %a = Artist  |  %b = Album        |  %d = Date   |  %s = Size    | %c = Composer
      %g = Genre   |  %n = Tracknumber  |  %t = Title  |  %l = Length
</pre>

# Example of NFO

<pre>
---------------------------------------------------------------------
         Atach Tatuq - La guerre des tuqs
---------------------------------------------------------------------

 Source...............: CD
 Year.................: 2002
 Ripper...............: RubyRipper (cdparanoia)
 Codec................: Free Lossless Audio Codec (FLAC)
 Version..............: 1.3.2
 Quality..............: Lossless
 Channels.............: Stereo / 44100 HZ / 16 Bit
 Tags.................: hip-hop, quebec
 Information..........:

 Ripped by............: baldurmen
 Posted by............: baldurmen
 News Server..........:
 News Group(s)........:


---------------------------------------------------------------------
                       Tracklisting
---------------------------------------------------------------------

01 - Ground Zero
02 - Le crachat et la chiure d'oiseau
03 - Y a trop d'shits
04 - Jazz fesse
05 - J'dérange personne
06 - Si tu fais le mal fais le bien
07 - Toast meats Naes
08 - HC978
09 - Introspection rapiale
10 - Athé musique
11 - Épisodes
12 - Groupeez
13 - Pad'presse à pad'price
14 - Toastdawg Anthem
15 - Gratè
16 - La guerre des tuqs

Playing Time.........: 00:54:20
Total Size...........: 296.7 MB

NFO generated on 03/09/2017 with genfo (https://gitlab.com/baldurmen/genfo)
</pre>
