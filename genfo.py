#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
genfo is a command-line tool that takes the information from FLAC
metadata to create a NFO file from a template.

Usage:
    genfo.py <scheme> <directory> <template> <output>
    genfo.py (-h | --help)
    genfo.py --version

Arguments:
    <directory>      The directory containing the album
    <scheme>         The tracklist scheme to use
    <template>       The blank NFO file to be filled
    <output>         The NFO file generated

Options:
    -h  --help       Shows the help screen
    --version        Outputs version information

    These are the tags you can use in the template file:

      {artist}       Name of the artist
      {album}        Name of the album
      {date}         Current date
      {genre}        Genre of the album
      {tracklist}    Tracklist, follow the scheme
      {total_size}   Total size in MB of the album
      {total_time}   Total playing time
      {version}      Version of flac your are using

    These are the options you can use to define the tracklist scheme:
      %a = Artist  |  %b = Album        |  %d = Date   |  %s = Size    | %c= Composer
      %g = Genre   |  %n = Tracknumber  |  %t = Title  |  %l = Length
"""
import sys

try:
    from docopt import docopt  # Creating command-line interface
except ImportError:
    sys.stderr.write("""
        %s is not installed: this program won't run correctly.
        """ % ("docopt"))

import subprocess
import re
import os
import datetime
import collections

__version__ = "2.1.6"

TAGS = dict(a='artist', b='album', c='composer',
            d='date', g='genre', l='length',
            n='tracknumber', s='size', t='title')


def filesize(filepath):
    """Grep the filesize."""
    size = os.stat(filepath)
    size = size.st_size / 1000000  # bytes to Mb
    size = round(size, 1)
    return size


def hms_convert(seconds):
    # pylint: disable=C0103
    """Convert seconds to H:M:S format."""
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    length = str("%02d:%02d:%02d") % (h, m, s)
    return length


def songlength(filepath):
    """Grep the length of a song."""
    pipe = subprocess.Popen(["metaflac", 
                            "--show-total-samples", 
                            "--show-sample-rate",
                            filepath],
                            stdout=subprocess.PIPE)
    output, error = pipe.communicate()
    output = output.decode('ascii') # binary to string
    tsample = int(output.split('\n', 1)[0])
    rsample = int(output.split('\n', 2)[1])
    seconds = round(tsample / rsample)
    length = hms_convert(seconds)
    return seconds, length


def scheme_processing(args):
    """Process the scheming arguments."""
    scheme = args["<scheme>"]
    scheme = re.sub('%%([%s])' % ''.join(TAGS.keys()),
                lambda m: '%%(%s)s' % TAGS[m.group(1)],
                scheme)
    return scheme


def fetch_metadata(filepath):
    """Fetch metadata and format it."""
    args = ["--show-tag=%s" % tag for tag in list(TAGS.values())]
    tags = ["%s=" % tag for tag in list(TAGS.values())]
    # make sure the keys exist. Prevents failure in case of incomplete metadata
    metadata = dict.fromkeys(list(TAGS.values()), '')
    pipe = subprocess.Popen(["metaflac"] + args + ["--show-vendor-tag"] + [filepath],
                            stdout=subprocess.PIPE)
    output, error = pipe.communicate()
    if pipe.returncode:
        raise IOError("metaflac failed: %s" % error)
    output = output.decode('utf-8') # binary to string
    output = output.splitlines()
    for tag in tags:
        for item in output:
            x = re.compile(re.escape(tag), re.IGNORECASE)
            if bool(re.match(x, item)) == True:
                tag = tag.replace("=", "")
                if tag == "tracknumber":
                    metadata[tag] = x.sub("", item).zfill(2)
                else:
                    metadata[tag] = x.sub("", item)
                metadata['version'] = output[len(output) - 1].strip('reference ')
    return metadata


def tracklist_dict(args):
    """Create the tracklist dictionary."""
    tracklist = dict()
    for dirname, _, filenames in os.walk(
                                   args["<directory>"],
                                   topdown=False):
        for filename in filenames:
            if os.path.splitext(filename)[1] == ".flac":
                filepath = os.path.join(dirname, filename)
                seconds, length = songlength(filepath)
                size = filesize(filepath)
                metadata = fetch_metadata(filepath)
                metadata['seconds'] = seconds
                metadata['length'] = length
                metadata['size'] = size
                key = metadata['tracknumber']
                tracklist[key] = metadata
    tracklist = collections.OrderedDict(sorted(list(tracklist.items()), key=lambda t:t[0]))
    return tracklist


def format_tracklist(tracklist, args):
    """Format the tracklist."""
    formatted_tracklist = []
    scheme = scheme_processing(args)
    for song in tracklist:
        line = scheme % tracklist[song]
        formatted_tracklist.append(line)
    formatted_tracklist = '\n'.join(formatted_tracklist)
    return formatted_tracklist


def totals(tracklist):
    """Get the total filesize and the total length."""
    tsize = 0
    tseconds = 0
    for song in tracklist:
        tsize += tracklist[song]['size']
        tseconds += tracklist[song]['seconds']
    tlength = hms_convert(tseconds)
    return tsize, tlength


def current_date():
    """Grep the current date."""
    today = datetime.date.today()
    date = today.strftime('%d/%m/%Y')
    return date


def nfo_builder(tracklist, args):
    """Put the NFO together and write it to a file."""
    formatted_tracklist = format_tracklist(tracklist, args)
    tsize, tlength = totals(tracklist)
    date = current_date()
    with open(args["<template>"], 'r', encoding='utf-8') as foo:
        nfofile = foo.read()
    nfofile = nfofile.replace("{artist}", tracklist['01']['artist'])
    nfofile = nfofile.replace("{album}", tracklist['01']['album'])
    nfofile = nfofile.replace("{genre}", tracklist['01']['genre'])
    nfofile = nfofile.replace("{year}", tracklist['01']['date'])
    nfofile = nfofile.replace("{version}", tracklist['01']['version'])
    nfofile = nfofile.replace("{tracklist}", formatted_tracklist)
    nfofile = nfofile.replace("{total_time}", tlength)
    nfofile = nfofile.replace("{total_size}", str(tsize))
    nfofile = nfofile.replace("{date}", date)
    with open(args["<output>"], 'w') as output:
        output.write(nfofile)


def main():
    """Main function."""
    args = docopt(__doc__, version="genfo %s" % __version__)
    if not os.path.isdir(args["<directory>"]):
        sys.exit("Error: %s is not a valid directory" % args["<directory>"])
    tracklist = tracklist_dict(args)
    nfo_builder(tracklist, args)


if __name__ == "__main__":
    main()
