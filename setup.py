#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Setup"""
from genfo import __version__
from setuptools import setup

setup(
    name='genfo',
    version=__version__,
    description="""A command-line tool templating engine that uses FLAC
                   metadata.""",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    author='Louis-Philippe Véronneau',
    python_requires='>=3.5.0',
    url='https://gitlab.com/baldurmen/genfo',
    install_requires='docopt',
    py_modules = ['genfo'],
    entry_points={
        'console_scripts': [
            'genfo = genfo:main',
        ]
    },
    license='GPLv3+',
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Multimedia :: Sound/Audio',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
    ],
)
